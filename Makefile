
run:
	docker-compose up

build:
	docker-compose build

restart:
	docker-compose restart

stop:
	docker-compose stop

clean:
	docker system prune -a --volumes