# docker-compose-nginx-mysql-php

## 这是个啥玩意

这玩意把 nginx、mysql、php 整合进了 docker-compose

## 使用方式

> 使用前提:需要先安装 docker 和 docker-compose

1. 配置 `.env` 文件，例:`mv env.example .env` or `cp env.example .env`
2. 启动命令 `docker-compose up` or `docker-compose up -d`
3. 地址 `http://127.0.0.1:8074` or `http://localhost:8074`
4. mysql 用户名: `root` 详见 `.env` 文件

## 使用容器版本

- [`nginx 1.21.0, mainline, 1, 1.21, latest`](https://github.com/nginxinc/docker-nginx/blob/f3fe494531f9b157d9c09ba509e412dace54cd4f/mainline/alpine/Dockerfile)
- [`php-fpm 7.4.20-cli-alpine3.13`, `7.4-cli-alpine3.13`, `7-cli-alpine3.13`, `7.4.20-alpine3.13`, `7.4-alpine3.13`, `7-alpine3.13`, `7.4.20-cli-alpine`, `7.4-cli-alpine`, `7-cli-alpine`, `7.4.20-alpine`, `7.4-alpine`, `7-alpine`](https://github.com/docker-library/php/blob/368453e2918b0a4a5ce488cc987c59c30e4ae4be/7.4/alpine3.13/fpm/Dockerfile)
- [`mysql 5.7.34, 5.7, 5`](https://github.com/docker-library/mysql/blob/b11f06b0d202e7b0f97b000e158fc4fc869d2194/5.7/Dockerfile.debian)
- [`redis 6.2.4-alpine, 6.2-alpine, 6-alpine, alpine, 6.2.4-alpine3.13, 6.2-alpine3.13, 6-alpine3.13, alpine3.13`](https://github.com/docker-library/redis/blob/4422738bea4a9d38971a87fc820525864bb5ff62/6.2/alpine/Dockerfile)
- [`memcached 1.6.9-alpine, 1.6-alpine, 1-alpine, alpine`](https://github.com/docker-library/memcached/blob/2f06a1eacb5ca1bbdcc1a1b57334897e04d7a270/alpine/Dockerfile)
